import React from "react";
import { Link } from "react-router-dom";
import logo from "./../../assets/logo.png"
import "./style.css";

export default function Navbar() {
  return (
    <div className="navbar">
      <nav>
        <div className="navbar-logo">
          <div className="logo-image" style={{backgroundImage:logo, backgroundSize:'50px 50px'}}></div>
          <div className="logo-text">CHRYSALIS</div>
        </div>
        <div className="navbar-menu">
          <ul>
            <li>
              <Link to={`me`}>
                <div className="menu-item">Me</div>
              </Link>
            </li>
            <li>
              <Link to={`messanges/`}>
                <div className="menu-item">Messanges</div>
              </Link>
            </li>
            <li>
              <Link to={`friends/`}>
                <div className="menu-item">Friends</div>
              </Link>
            </li>
            <li>
              <Link to={`settings/`}>
                <div className="menu-item">Settings</div>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}
