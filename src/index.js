import React from 'react';
import ReactDOM from 'react-dom/client';
import { createBrowserRouter, Outlet, RouterProvider } from 'react-router-dom';
import './index.css';
import App from './App';
import ErrorPage from './components/ErrorRoute/ErrorPage';
import Me from './components/Root/Me/Me';
import Messanges from './components/Root/Messanges/Messanges';
import Navbar from './components/Navbar/Navbar';
import Friends from './components/Root/Friends/Friends';
import Settings from './components/Root/Settings/Settings';

const router = createBrowserRouter([
  {
  path: '/',
  element: <AppLayout />,
  errorElement:<ErrorPage />,
  children:[
    {
      path:'/me',
      element:<Me />
    },
    {
      path:'/messanges',
      element:<Messanges />
    },
    {
      path:'/friends',
      element:<Friends />
    },
    {
      path:'/settings',
      element:<Settings />
    },
  ]
  },
])

function AppLayout(){
  return (
    <>
      <Navbar/>
      <Outlet/>
    </>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router}>
      <App />
    </RouterProvider>
  </React.StrictMode>
);
